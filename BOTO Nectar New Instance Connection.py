{\rtf1\ansi\ansicpg1252\cocoartf1504\cocoasubrtf830
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\paperw11900\paperh16840\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0

\f0\fs24 \cf0 #USISNG BOTO TO CREATE INSTANCE, VOLUME, SNAPSHOT\
import boto\
from boto.ec2.regioninfo import RegionInfo\
region = RegionInfo(name='melbourne', endpoint='nova.rc.nectar.org.au')\
ec2_conn = boto.connect_ec2(aws_access_key_id='0f96560c890442c0baa25b8270577108',\
 aws_secret_access_key='9fd969cb32254918ba921401ccea83a8',\
 is_secure=True,\
 region=region,\
 port=8773,\
 path='/services/Cloud',\
 validate_certs=False)\
\
#LISTING ALL IMAGES\
images = ec2_conn.get_all_images()\
for img in images:\
 print('Image id: \{\}, image name: \{\}'.format(img.id, img.name)) \
print(dir(ec2_conn))\
\
#CREATE NEW INSTANCE\
reservation = ec2_conn.run_instances('ami-190a1773',\
 key_name='nid',\
 instance_type='m2.medium',\
 security_groups=['default'],\
  placement='melbourne-qh2')\
\
#RESVING SPACE IN NECTAR\
instance = reservation.instances[0]\
print('New instance \{\} has been created.'.format(instance.id)) \
reservations = ec2_conn.get_all_reservations() \
\
print('Index\\tID\\t\\tInstance')\
for idx, res in enumerate(reservations):\
 print('\{\}\\t\{\}\\t\{\}'.format(idx, res.id, res.instances)) \
\
print('\\nID: \{\}\\tIP: \{\}\\tPlacement: \{\}'.format(reservations[0].id,\
 reservations[1].instances[0].private_ip_address,\
 reservations[1].instances[0].placement)) \
\
\
#NEW INSTANCE ID\
print(instance.id)\
\
#CREATIG NEW VOLUME\
vol_req=ec2_conn.create_volume(5,'melbourne-qh2')\
\
curr_vols=ec2_conn.get_all_volumes([vol_req.id])\
print('Volume status: \{\}, volume AZ: \{\}'.format(curr_vols[0].status, curr_vols[0].zone))\
\
#ATTACHING VOLUME\
ec2_conn.attach_volume(vol_req.id,instance.id,'/dev/vdc')\
\
#CREATE VOLUME SNAPSHOT\
snapshot=ec2_conn.create_snapshot(vol_req.id,'Snapshot demo')\
\
#USING PARAMIKO TO CONNECT TO THE NEW INSTANCE\
import paramiko\
k = paramiko.RSAKey.from_private_key_file("nid.key")\
c = paramiko.SSHClient()\
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())\
print ("connecting")\
\
#CONNECTING TO NEW INSTANCE IP\
c.connect( \'93ip_new_instance_from_nectar\'94, port=22, username = "ubuntu", pkey = k )\
print ("connected")}