#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  7 19:07:01 2018

@author: yongzilau
"""


import ijson
import json
import re
from textblob import TextBlob
import couchdb

DATAFILE = 'a6.json'
dbname = "aus_tweet2"

server = couchdb.Server("http://admin:admin@127.0.0.1:5985/")
#server = couchdb.Server("http://admin:admin@localhost:9000/")
#server = couchdb.Server("http://yongzil:password@localhost:5984/")

dberror = 0
with open(DATAFILE,"r") as f2:
    objects = ijson.items(f2, 'features.item')
    #iterate through datafile
    for i, o in enumerate(objects):
        #print(o)
        #print(o['properties']['text'])
        if(i<357465):
            continue
        
        o['geometry']['coordinates'][0] = float(o['geometry']['coordinates'][0])
        o['geometry']['coordinates'][1] = float(o['geometry']['coordinates'][1])
        text = o['properties']['text']
        cleantext = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", text).split())
        polarity = TextBlob(cleantext).sentiment.polarity
        #print(polarity)
        o['properties']['polarity'] = polarity
        
        #print(type(o))
        o1 = json.dumps(o)
        try:
            if dbname in server:
                db = server[dbname]
                #db[docid] = final_data
                db.save(o)
            else:
                db = server.create(dbname)
                #db[docid] = final_data
                db.save(o)
        except BaseException as e:
            dberror += 1
            print(e)
        if(i%5000==0):
            print('processed {} tweets'.format(i))
print('import errors : {}'.format(dberror))
print('imported:{} tweets'.format(i-dberror))